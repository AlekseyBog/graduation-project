package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.*;
import jakarta.persistence.Column;
import jakarta.persistence.Enumerated;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddServiceDTO extends GenericDTO{

    private String addServiceTitle;

    private TypeService typeService;

    private TypePrice typePrice;

    private Long priceService;

    private Set<Long> bookingsIds;

    private boolean isDeleted;

    public AddServiceDTO(AddService addService) {
        AddServiceDTO addServiceDTO = new AddServiceDTO();

        addServiceDTO.setAddServiceTitle(addService.getAddServiceTitle());
        addServiceDTO.setTypeService(addService.getTypeService());
        addServiceDTO.setTypePrice(addService.getTypePrice());
        addServiceDTO.setPriceService(addService.getPriceService());
        Set<Booking> bookings = addService.getBookings();
        Set<Long> bookingIds = new HashSet<>();
        if (bookings != null && bookings.size() > 0) {
            bookings.forEach(b -> bookingIds.add(b.getId()));
        }
        addServiceDTO.setBookingsIds(bookingsIds);
    }
}
