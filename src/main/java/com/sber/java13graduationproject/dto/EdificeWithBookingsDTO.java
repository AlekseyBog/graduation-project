package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.Edifice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EdificeWithBookingsDTO
        extends EdificeDTO {

    private Set<BookingDTO> bookings;
    public EdificeWithBookingsDTO(Edifice edifice, Set<BookingDTO> bookings) {
        super(edifice);
        this.bookings = bookings;
    }
}
