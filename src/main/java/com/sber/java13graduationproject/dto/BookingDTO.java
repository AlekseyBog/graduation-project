package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.mapper.EdificeMapper;
import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.model.Booking;
import com.sber.java13graduationproject.model.User;
import lombok.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BookingDTO extends GenericDTO {

    private Long edificeId;

    private LocalDate rentDate;

    private LocalDate returnDate;

    private Long priceBooking;

    private String onlineCopyPath;

    private Set<Long> usersIds;

    private Set<Long> addServicesIds;

    private EdificeDTO edificeDTO;

//    public BookingDTO(Booking booking) {
//        BookingDTO bookingDTO = new BookingDTO();
//
//        bookingDTO.setEdificeId(booking.getEdifice().getId());
//        bookingDTO.setRentDate(booking.getRentDate());
//        bookingDTO.setReturnDate(booking.getReturnDate());
//        bookingDTO.setPriceBooking(booking.getPriceBooking());
//        Set<User> users = booking.getUsers();
//        Set<Long> usersIds = new HashSet<>();
//        if (users != null && users.size() > 0) {
//            users.forEach(u -> usersIds.add(u.getId()));
//        }
//        bookingDTO.setUsersIds(usersIds);
//        Set<AddService> addServices = booking.getAddServices();
//        Set<Long> addServIds = new HashSet<>();
//        if (addServices != null && addServices.size() > 0) {
//            addServices.forEach(a -> addServIds.add(a.getId()));
//        }
//        bookingDTO.setAddServicesIds(addServIds);
//        bookingDTO.setEdificeDTO();
//    }
}
