package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.Booking;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.model.TypeEdifice;
import com.sber.java13graduationproject.model.TypePrice;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EdificeDTO
        extends GenericDTO {

    private String edificeTitle;

    private TypeEdifice typeEdifice;

    private String description;

    private TypePrice typePrice;

    private Long priceEdifice;

    private Set<Long> bookingsIds;

    private boolean isDeleted;

    public EdificeDTO(Edifice edifice) {
        EdificeDTO edificeDTO = new EdificeDTO();

        edificeDTO.setEdificeTitle(edifice.getEdificeTitle());
        edificeDTO.setTypeEdifice(edifice.getTypeEdifice());
        edificeDTO.setDescription(edifice.getDescription());
        edificeDTO.setTypePrice(edifice.getTypePrice());
        edificeDTO.setPriceEdifice(edifice.getPriceEdifice());
        Set<Booking> bookings = edifice.getBookings();
        Set<Long> bookingIds = new HashSet<>();
        if (bookings != null && bookings.size() > 0) {
            bookings.forEach(b -> bookingIds.add(b.getId()));
        }
        edificeDTO.setBookingsIds(bookingsIds);
    }
}
