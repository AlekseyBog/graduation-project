package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.TypeService;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AddServiceSearchDTO {

    private String addServiceTitle;

    private TypeService typeService;

}
