package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.Booking;
import com.sber.java13graduationproject.model.Edifice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingWithUsersAndServicesDTO extends BookingDTO {

    private Set<UserDTO> users;

    private Set<AddServiceDTO> addServices;

}
