package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.model.Edifice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddServiceWithBookingsDTO
        extends AddServiceDTO {
    private Set<BookingDTO> bookings;
    public AddServiceWithBookingsDTO(AddService addService,
                                     Set<BookingDTO> bookings) {
        super(addService);
        this.bookings = bookings;
    }
}
