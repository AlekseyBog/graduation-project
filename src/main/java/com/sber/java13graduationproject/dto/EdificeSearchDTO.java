package com.sber.java13graduationproject.dto;

import com.sber.java13graduationproject.model.TypeEdifice;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EdificeSearchDTO {

    private String edificeTitle;

    private TypeEdifice typeEdifice;
}
