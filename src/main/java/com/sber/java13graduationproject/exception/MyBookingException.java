package com.sber.java13graduationproject.exception;

public class MyBookingException extends Exception{
    public MyBookingException(String message) {
        super(message);
    }
}
