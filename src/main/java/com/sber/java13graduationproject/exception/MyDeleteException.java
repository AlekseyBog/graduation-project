package com.sber.java13graduationproject.exception;

public class MyDeleteException
      extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
