package com.sber.java13graduationproject.MVC.controller;

import com.sber.java13graduationproject.dto.*;
import com.sber.java13graduationproject.exception.MyBookingException;
import com.sber.java13graduationproject.mapper.BookingAllMapper;
import com.sber.java13graduationproject.mapper.BookingMapper;
import com.sber.java13graduationproject.service.AddServiceService;
import com.sber.java13graduationproject.service.BookingService;
import com.sber.java13graduationproject.service.EdificeService;
import com.sber.java13graduationproject.service.UserService;
import com.sber.java13graduationproject.service.userdetails.CustomUserDetails;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

@Hidden
@Controller
@RequestMapping("/bookings")
@Slf4j
public class MVCBookingController {

    private final BookingService bookingService;

    private final EdificeService edificeService;

    private final AddServiceService addServiceService;

    private final UserService userService;


    public MVCBookingController(BookingService bookingService,
                                EdificeService edificeService,
                                AddServiceService addServiceService,
                                UserService userService) {
        this.bookingService = bookingService;
        this.edificeService = edificeService;
        this.addServiceService = addServiceService;
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("booking", bookingService.getById(id));
        return "bookings/viewBooking";
    }

    @GetMapping("/edifices/{edificeId}")
    public String rentEdifice(@PathVariable Long edificeId,
                              @ModelAttribute(name = "exception") final String exception,
                              BindingResult bindingResult,
                              Model model) {
        model.addAttribute("edifice", edificeService.getOne(edificeId));
        model.addAttribute("addService", addServiceService.listAll());
        model.addAttribute("exception", exception);
        model.addAttribute("rentEdificeForm", new BookingWithUsersAndServicesDTO());
        model.addAttribute("rentAll", bookingService.getAllBookingsByEdificeIdBeforNow(edificeId));
        return "userEdifices/bookingEdifice";
    }

    @PostMapping("/edifices")
    public String rentEdifice(@ModelAttribute("rentEdificeForm") BookingWithUsersAndServicesDTO bookingDTO,
                              BindingResult bindingResult,
                              Model model) throws MyBookingException {
        model.addAttribute("rentAll", bookingService.getAllBookingsByEdificeIdBeforNow(bookingDTO.getEdificeId()));
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        bookingDTO.setUsersIds(new HashSet<>(Arrays.asList(Long.valueOf(customUserDetails.getUserId()))));
        bookingService.getAllBookingsByEdifice(bookingDTO.getEdificeId());
        for (BookingWithUsersAndServicesDTO booking : bookingService.getAllBookingsByEdifice(bookingDTO.getEdificeId())) {
            LocalDate tRD = bookingDTO.getRentDate();
            LocalDate sRD = booking.getRentDate();
            LocalDate sRnD = booking.getReturnDate();
            LocalDate tRnD = bookingDTO.getReturnDate();
            boolean result = false;
            log.info((!tRD.isBefore(sRD) && tRD.isBefore(sRnD)) + "LOG DATE" + (!tRD.isBefore(sRD)) + (tRD.isBefore(sRnD)));
            if (tRD.isBefore(sRD) && !tRnD.isAfter(sRD)) {
                result = true;
            }
            if (tRD.isAfter(sRD) && !sRnD.isAfter(tRD)) {
                result = true;
            }
            if (tRD.isEqual(sRD)) {
                result = false;
            }
            if (!result) {
                model.addAttribute("edifice", edificeService.getOne(bookingDTO.getEdificeId()));
                bindingResult.rejectValue("returnDate", "error.returnDate", "На выбранные даты объект забронирован другим пользователем");
                bindingResult.rejectValue("rentDate", "error.rentDate", "Пожалуйста, обратитесь к менеджеру для уточнения свободных дат");
                return "userEdifices/bookingEdifice";
            }
        }
        bookingService.rentEdifice(bookingDTO);
        return "redirect:/bookings/user-edifices/" + customUserDetails.getUserId();
    }

    @GetMapping("/user-edifices/{id}")
    public String userBookings(@RequestParam(value = "page", defaultValue = "1") int page,
                               @RequestParam(value = "size", defaultValue = "5") int pageSize,
                               @PathVariable Long id,
                               Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<BookingWithUsersAndServicesDTO> bookingDTOPage = bookingService.listUserBookingsEdifices(id, pageRequest);
        model.addAttribute("bookingsEdifices", bookingDTOPage);
        model.addAttribute("userId", id);
        model.addAttribute("addServices", addServiceService.listAll());
        return "userEdifices/viewAllBookings";
    }

    @GetMapping("/add-service/{bookingId}")
    public String addService(@PathVariable Long bookingId,
                             Model model) {
        model.addAttribute("addServices", addServiceService.listAll());
        model.addAttribute("bookingId", bookingId);
        model.addAttribute("booking", bookingService.getOne(bookingId));
        return "bookings/addServices";
    }

    @PostMapping("/add-service/{bookingId}")
    public String addService(@PathVariable Long bookingId,
                             @ModelAttribute("bookingServiceForm") AddServiceDTO addServiceDTO,
                             Model model) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("bookingServiceForm", addServiceDTO);
        bookingService.addServicesOne(addServiceDTO, bookingId);
        return "redirect:/bookings/user-edifices/" + customUserDetails.getUserId();
    }

    @GetMapping("/add-user/{bookingId}")
    public String addUser(@PathVariable Long bookingId,
                          Model model) {
        model.addAttribute("bookingUsers", userService.listAll());
        model.addAttribute("bookingId", bookingId);
        model.addAttribute("booking", bookingService.getOne(bookingId));
        return "bookings/addUser";
    }

    @PostMapping("/add-user/{bookingId}")
    public String addUser(@PathVariable Long bookingId,
                          @ModelAttribute("bookingUserForm") UserDTO userDTO,
                          Model model) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("bookingUserForm", userDTO);
        bookingService.addUserOne(userDTO, bookingId);
        return "redirect:/bookings/user-edifices/" + customUserDetails.getUserId();
    }


    @ExceptionHandler({MyBookingException.class})
    public RedirectView handleError(Exception ex,
                                    RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/bookings/edifices/", true);
    }

    //    bookingsEdifices
    @GetMapping("/activeForToDay")
    public String activeForToDay(@RequestParam(value = "page", defaultValue = "1") int page,
                                 @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                 Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<BookingWithUsersAndServicesDTO> bookingDTOPage = bookingService.listBookingsToDay(pageRequest);
        Page<BookingWithUsersAndServicesDTO> bookingDTOPage1 = bookingService.listAllBookingsAfterDay(pageRequest);
        model.addAttribute("bookingsEdifices", bookingDTOPage);
        model.addAttribute("bookingsEdifices1", bookingDTOPage1);
        return "bookings/activeForToDay";
    }

    @GetMapping("/edifices/addFile/{id}")
    public String addFile(@PathVariable Long id,
                          Model model) {
        log.info(id + "Loooooooooogsfafeqrgf");
        log.info(bookingService.getOne(id) + "Loooooooooogsfafeqrgf");
        model.addAttribute("bookingFile", bookingService.getOne(id));
        return "bookings/addBookingFile";
    }

    @PostMapping("/edifices/addFile/{id}")
    public String addFile(@PathVariable Long id,
                          @RequestParam MultipartFile file,
                          Model model) {
        model.addAttribute("bookingFileForm", bookingService.getOne(id));
        log.info("POSTkbkgj;dbgsj;trnb;gkrtj");
        if (file != null && file.getSize() > 0) {
            bookingService.create(bookingService.getOne(id), file);
        }
        log.info(bookingService.getOne(id).getId() + "Loooooooooogsfafeqrgf");
        return "redirect:/bookings/" + id;
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadFileBooking(@Param(value = "bookingId") Long bookingId) throws IOException {
        BookingDTO bookingDTO = bookingService.getOne(bookingId);
        Path path = Paths.get(bookingDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }
}
