package com.sber.java13graduationproject.MVC.controller;

import com.sber.java13graduationproject.dto.*;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.service.AddServiceService;
import com.sber.java13graduationproject.service.EdificeService;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Hidden
@Controller
@RequestMapping("addServices")
@Slf4j
public class MVCAddServiceController {

    private final AddServiceService addServiceService;

    public MVCAddServiceController(AddServiceService addServiceService) {
        this.addServiceService = addServiceService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute(name = "exception") final String exception,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "addServiceTitle"));
        Page<AddServiceWithBookingsDTO> result = addServiceService.getAllAddServicesWithBookings(pageRequest);
        model.addAttribute("addServices", result);
        model.addAttribute("exception", exception);
        return "addServices/viewAllAddServices";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("addService", addServiceService.getAddServiceWithBookings(id));
        return "addServices/viewAddService";
    }

    @GetMapping("/add")
    public String create() {
        return "addServices/addAddService";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("addServiceForm") AddServiceDTO addServiceDTO) {
        addServiceService.create(addServiceDTO);
        return "redirect:/addServices";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("addService", addServiceService.getOne(id));
        return "addServices/updateAddService";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("addServiceForm") AddServiceDTO addServiceDTO) {
        addServiceService.update(addServiceDTO);
        return "redirect:/addServices";
    }

    @PostMapping("/search")
    public String searchAddServices(@RequestParam(value = "page", defaultValue = "1") int page,
                                 @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                 @ModelAttribute("addServiceSearchForm") AddServiceSearchDTO addServiceSearchDTO,
                                 Model model) {
        log.info(addServiceSearchDTO.toString());
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("addServices", addServiceService.findAddServices(addServiceSearchDTO, pageRequest));
        return "addServices/viewAllAddServices";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        addServiceService.deleteSoft(id);
        return "redirect:/addServices";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        addServiceService.restore(id);
        return "redirect:/addServices";
    }

    @ExceptionHandler({MyDeleteException.class, AccessDeniedException.class})
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/addServices", true);
    }
}
