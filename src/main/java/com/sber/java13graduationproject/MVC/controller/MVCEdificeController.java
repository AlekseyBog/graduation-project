package com.sber.java13graduationproject.MVC.controller;

import com.sber.java13graduationproject.dto.EdificeDTO;
import com.sber.java13graduationproject.dto.EdificeSearchDTO;
import com.sber.java13graduationproject.dto.EdificeWithBookingsDTO;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.service.EdificeService;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
@Hidden
@Controller
@RequestMapping("edifices")
@Slf4j
public class MVCEdificeController {

    private final EdificeService edificeService;

    public MVCEdificeController(EdificeService edificeService) {
        this.edificeService = edificeService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute(name = "exception") final String exception,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "edificeTitle"));
        Page<EdificeWithBookingsDTO> result = edificeService.getAllEdificesWithBookings(pageRequest);
        model.addAttribute("edifices", result);
        model.addAttribute("exception", exception);
        return "edifices/viewAllEdifices";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("edifice", edificeService.getEdificeWithBookings(id));
        return "edifices/viewEdifice";
    }

    @GetMapping("/add")
    public String create() {
        return "edifices/addEdifice";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("edificeForm") EdificeDTO edificeDTO) {
        edificeService.create(edificeDTO);
        return "redirect:/edifices";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("edifice", edificeService.getOne(id));
        return "edifices/updateEdifice";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("edificeForm") EdificeDTO edificeDTO) {
            edificeService.update(edificeDTO);
        return "redirect:/edifices";
    }

    @PostMapping("/search")
    public String searchEdifices(@RequestParam(value = "page", defaultValue = "1") int page,
                                 @RequestParam(value = "size", defaultValue = "5") int pageSize,
                                 @ModelAttribute("edificeSearchForm") EdificeSearchDTO edificeSearchDTO,
                                 Model model) {
        log.info(edificeSearchDTO.toString());
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("edifices", edificeService.findEdifices(edificeSearchDTO, pageRequest));
        return "edifices/viewAllEdifices";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        edificeService.deleteSoft(id);
        return "redirect:/edifices";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        edificeService.restore(id);
        return "redirect:/edifices";
    }

    @ExceptionHandler({MyDeleteException.class, AccessDeniedException.class})
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/edifices", true);
    }
}

