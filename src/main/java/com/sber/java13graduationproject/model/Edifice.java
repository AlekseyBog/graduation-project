package com.sber.java13graduationproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.domain.Range;

import java.util.Set;

@Entity
@Table(name = "edifice")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "edifice_seq", allocationSize = 1)
public class Edifice extends GenericModel{

    @Column(name = "title", nullable = false)
    private String edificeTitle;

    @Column(name = "type_edifice")
    @Enumerated
    private TypeEdifice typeEdifice;

    @Column(name = "description")
    private String description;

    @Column(name = "type_price")
    @Enumerated
    private TypePrice typePrice;

    @Column(name = "price_edifice")
    private Long priceEdifice;

    @OneToMany(mappedBy = "edifice", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Booking> bookings;

}
