package com.sber.java13graduationproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "add_services")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "add_services_seq", allocationSize = 1)
public class AddService extends GenericModel {

    @Column(name = "title", nullable = false)
    private String addServiceTitle;

    @Column(name = "type_service")
    @Enumerated
    private TypeService typeService;

    @Column(name = "type_price")
    @Enumerated
    private TypePrice typePrice;

    @Column(name = "price_service")
    private Long priceService;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "bookings_add_services",
            joinColumns = @JoinColumn(name = "add_service_id"),
            foreignKey = @ForeignKey(name = "FK_ADD_SERVICE_BOOKING"),
            inverseJoinColumns = @JoinColumn(name = "booking_id"),
            inverseForeignKey = @ForeignKey(name = "FK_BOOKING_ADD_SERVICE"))
    private Set<Booking> bookings;
}
