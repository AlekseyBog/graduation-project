package com.sber.java13graduationproject.model;

public enum TypePrice {

    PER_HOUR("За 1 час"),
    PER_DAY("За 1 сутки"),
    PER_PIECE("За 1 шт.");

    private final String typePriceTextDisplay;

    TypePrice(String text){
        this.typePriceTextDisplay = text;
    }

    public String getTypePriceTextDisplay() {
        return this.typePriceTextDisplay;
    }
}
