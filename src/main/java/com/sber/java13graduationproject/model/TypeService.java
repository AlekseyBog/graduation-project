package com.sber.java13graduationproject.model;

import java.util.EnumSet;

public enum TypeService {

    OUTFIT("Принадлежности"),
    BREAKFAST("Завтрак"),
    LUNCH("Обед"),
    DINNER("Ужин");

    private static final EnumSet<TypeService> food = EnumSet.of(BREAKFAST, LUNCH, DINNER);

    private final String typeServiceTextDisplay;

    TypeService(String text) {
        this.typeServiceTextDisplay = text;

    }

    public String getTypeServiceTextDisplay() {
        return this.typeServiceTextDisplay;
    }


}
