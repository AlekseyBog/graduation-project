package com.sber.java13graduationproject.model;

public enum TypeEdifice {

    HOUSE("Дом"),
    HOUSE_BATH("Дом-Баня"),
    BATH("Баня"),
    SUMMERHOUSE("Беседка");

    private final String typeEdificeTextDisplay;

    TypeEdifice(String text) {
        this.typeEdificeTextDisplay = text;
    }

    public String getTypeEdificeTextDisplay() {
        return this.typeEdificeTextDisplay;
    }
}
