package com.sber.java13graduationproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "bookings")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "bookings_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Booking extends GenericModel {

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "edifice_id", foreignKey = @ForeignKey(name = "FK_BOOKING_EDIFICE"))
    private Edifice edifice;

    @Column(name = "rent_date")
    private LocalDate rentDate;

    @Column(name = "return_date")
    private LocalDate returnDate;

    @Column(name = "price_booking")
    private Long priceBooking;

    @Column(name = "online_copy_path")
    private String onlineCopyPath;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "bookings_users",
            joinColumns = @JoinColumn(name = "booking_id"),
            foreignKey = @ForeignKey(name = "FK_BOOKING_USER"),
            inverseJoinColumns = @JoinColumn(name = "user_id"),
            inverseForeignKey = @ForeignKey(name = "FK_USER_BOOKING"))
    private Set<User> users;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "bookings_add_services",
            joinColumns = @JoinColumn(name = "booking_id"),
            foreignKey = @ForeignKey(name = "FK_BOOKING_ADD_SERVICE"),
            inverseJoinColumns = @JoinColumn(name = "add_service_id"),
            inverseForeignKey = @ForeignKey(name = "FK_ADD_SERVICE_BOOKING"))
    private Set<AddService> addServices;
}
