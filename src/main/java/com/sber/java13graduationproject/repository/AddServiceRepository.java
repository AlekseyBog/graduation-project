package com.sber.java13graduationproject.repository;

import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.model.Edifice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AddServiceRepository
        extends GenericRepository<AddService> {

    @Query(nativeQuery = true,
            value = """
                    select distinct a.*
                    from add_services a
                    where a.title ilike '%' || btrim(coalesce(:title, a.title)) || '%'
                      and cast(a.type_service as char) like coalesce(:type, '%')
                         """)
    Page<AddService> searchAddService(@Param(value = "type") String type,
                                 @Param(value = "title") String title,
                                 Pageable pageable);

    @Query(nativeQuery = true,
            value = """
            select case when count(*) > 0 then false else true end
                from add_services a
                left join bookings_add_services bas on a.id = bas.add_service_id
                join bookings b on bas.booking_id = b.id
            where a.id = :id and b.return_date > now()
            group by a.id
            """)
    boolean checkAddServiceForDeletion(final Long id);

    Page<AddService> findAllByIsDeletedFalse(Pageable pageable);
}
