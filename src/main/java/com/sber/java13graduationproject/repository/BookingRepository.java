package com.sber.java13graduationproject.repository;

import com.sber.java13graduationproject.dto.BookingWithUsersAndServicesDTO;
import com.sber.java13graduationproject.model.Booking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface BookingRepository
        extends GenericRepository<Booking> {

    @Query(nativeQuery = true,
            value = """
                    select distinct b.*
                    from bookings b left join bookings_users bu on b.id = bu.booking_id
                    join users u on u.id = bu.user_id
                    where u.id = :userId
                                        """)
    Page<Booking> getBookingByUserId(Long userId, Pageable pageable);

    @Query(nativeQuery = true,
            value = """
                    update bookings set id = :id where id = :id
                    """)
    void update(Long id);


    @Query(nativeQuery = true,
            value = """
                    insert into bookings_add_services (booking_id, add_service_id) values (:bookingId, :id)
                    """)
    void updateBookingAddService(Long id, Long bookingId);

    @Query(nativeQuery = true,
            value = """
                    insert into bookings_users(user_id, booking_id) values (:id, :bookingId)
                    """)
    void updateBookingAddUser(Long id, Long bookingId);

    @Query(nativeQuery = true,
            value = """
                    select distinct b.*
                    from bookings b join edifice e on e.id = b.edifice_id
                    where edifice_id = :edfId
                    """)
    List<Booking> getBookingByEdifice(Long edfId);

    @Query(nativeQuery = true,
            value = """
                    update bookings
                    set price_booking = price_booking + :priceService
                    where id = :bookingId
                    """)
    void updatePriceBooking(Long priceService, Long bookingId);

    @Query(nativeQuery = true,
            value = """
                    select distinct b.*
                    from bookings b join edifice e on e.id = b.edifice_id
                    where edifice_id = :edificeId
                    and rent_date > (now() - interval '3 day')
                    """)
    List<Booking> getBookingByEdificeIdBeforNow(Long edificeId);

    @Query(nativeQuery = true,
            value = """
                    select distinct b.*
                    from bookings b
                    where now() >= rent_date
                    and now() <= return_date
                    """)
    Page<Booking> getBookingToDay(Pageable pageable);

    @Query(nativeQuery = true,
            value = """
                    select distinct b.*
                    from bookings b
                    where now() <= return_date
                    """)
    Page<Booking> getBookingAfterDay(Pageable pageable);

    @Query(nativeQuery = true,
            value = """
                    update bookings
                    set online_copy_path = :fileName
                    where id = :id
                    """)
    void updateFileById(String fileName, Long id);
}
