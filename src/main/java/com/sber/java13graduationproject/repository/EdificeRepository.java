package com.sber.java13graduationproject.repository;

import com.sber.java13graduationproject.dto.EdificeWithBookingsDTO;
import com.sber.java13graduationproject.model.Edifice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


@Repository
public interface EdificeRepository
        extends GenericRepository<Edifice> {

    @Query(nativeQuery = true,
            value = """
                    select distinct e.*
                    from edifice e
                    where e.title ilike '%' || btrim(coalesce(:title, e.title)) || '%'
                      and cast(e.type_edifice as char) like coalesce(:type, '%')
                         """)
    Page<Edifice> searchEdifices(@Param(value = "type") String type,
                                 @Param(value = "title") String title,
                                 Pageable pageable);

    @Query("""
            select case when count(e) > 0 then false else true end
                from Edifice e
            join Booking b on e.id = b.edifice.id
            where e.id = :id and b.returnDate >= now()
            group by e.id
            """)
    boolean checkEdificeForDeletion(final Long id);

    Page<Edifice> findAllByIsDeletedFalse(Pageable pageable);

}
