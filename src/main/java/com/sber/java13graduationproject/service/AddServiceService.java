package com.sber.java13graduationproject.service;

import com.sber.java13graduationproject.constants.Errors;
import com.sber.java13graduationproject.dto.AddServiceDTO;
import com.sber.java13graduationproject.dto.AddServiceSearchDTO;
import com.sber.java13graduationproject.dto.AddServiceWithBookingsDTO;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.mapper.AddServiceMapper;
import com.sber.java13graduationproject.mapper.AddServiceWithBookingsMapper;
import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.repository.AddServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.AopInvocationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class AddServiceService
        extends GenericService<AddService, AddServiceDTO> {

    private final AddServiceRepository addServiceRepository;

    private final AddServiceWithBookingsMapper addServiceWithBookingsMapper;

    public AddServiceService(AddServiceRepository addServiceRepository,
                             AddServiceMapper addServiceMapper,
                             AddServiceWithBookingsMapper addServiceWithBookingsMapper) {
        super(addServiceRepository, addServiceMapper);
        this.addServiceRepository = addServiceRepository;
        this.addServiceWithBookingsMapper = addServiceWithBookingsMapper;
    }

    public Page<AddServiceWithBookingsDTO> getAllAddServicesWithBookings(Pageable pageable) {
        Page<AddService> addServicesPaginated = addServiceRepository.findAll(pageable);
        List<AddServiceWithBookingsDTO> result = addServiceWithBookingsMapper.toDTOs(addServicesPaginated.getContent());
        return new PageImpl<>(result, pageable, addServicesPaginated.getTotalElements());
    }

    public Page<AddServiceWithBookingsDTO> getAllNotDeletedAddServicesWithBookings(Pageable pageable) {
        Page<AddService> addServicesPaginated = addServiceRepository.findAllByIsDeletedFalse(pageable);
        List<AddServiceWithBookingsDTO> result = addServiceWithBookingsMapper.toDTOs(addServicesPaginated.getContent());
        return new PageImpl<>(result, pageable, addServicesPaginated.getTotalElements());
    }

    public AddServiceWithBookingsDTO getAddServiceWithBookings(Long id) {
        return addServiceWithBookingsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<AddServiceWithBookingsDTO> findAddServices(AddServiceSearchDTO addServiceSearchDTO, Pageable pageable) {
        String typeAddService = addServiceSearchDTO.getTypeService() != null ? String.valueOf(addServiceSearchDTO.getTypeService().ordinal()) : null;
        Page<AddService> addServicesPaginated = addServiceRepository.searchAddService(typeAddService,
                addServiceSearchDTO.getAddServiceTitle(), pageable);
        List<AddServiceWithBookingsDTO> result = addServiceWithBookingsMapper.toDTOs(addServicesPaginated.getContent());
        return new PageImpl<>(result, pageable, addServicesPaginated.getTotalElements());
    }

    public AddServiceDTO create(final AddServiceDTO object) {
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(addServiceRepository.save(mapper.toEntity(object)));
    }

    public AddServiceDTO update(final AddServiceDTO object) {
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(addServiceRepository.save(mapper.toEntity(object)));
    }

    @Override
    public void deleteSoft(Long id) throws MyDeleteException {
        AddService addService = addServiceRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Объекта с заданным ID = " + id + " не существует"));
        boolean addServiceCanBeDeleted = false;
        try {
            addServiceRepository.checkAddServiceForDeletion(id);
        } catch (AopInvocationException exception) {
            addServiceCanBeDeleted = true;
        }
        if (addServiceCanBeDeleted) {
            markAsDeleted(addService);
            addServiceRepository.save(addService);
        } else {
            throw new MyDeleteException(Errors.AddServices.ADD_SERVICE_DELETE_ERROR);
        }
    }

    public void restore(Long objectId) {
        AddService addService = addServiceRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Объекта с заданным ID = " + objectId + " не существует"));
        unMarkAsDeleted(addService);
        addServiceRepository.save(addService);
    }
}
