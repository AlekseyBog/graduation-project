package com.sber.java13graduationproject.service;

import com.sber.java13graduationproject.dto.*;
import com.sber.java13graduationproject.mapper.AddServiceMapper;
import com.sber.java13graduationproject.mapper.BookingAllMapper;
import com.sber.java13graduationproject.mapper.BookingMapper;
import com.sber.java13graduationproject.model.Booking;
import com.sber.java13graduationproject.repository.AddServiceRepository;
import com.sber.java13graduationproject.repository.BookingRepository;
import com.sber.java13graduationproject.utils.FileHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class BookingService
        extends GenericService<Booking, BookingDTO> {

    private final EdificeService edificeService;

    private final BookingAllMapper bookingAllMapper;

    private final BookingRepository bookingRepository;

    private final AddServiceRepository addServiceRepository;


    private final BookingMapper bookingMapper;

    public BookingService(BookingRepository repository,
                          EdificeService edificeService,
                          BookingMapper bookingMapper,
                          BookingAllMapper bookingAllMapper,
                          AddServiceRepository addServiceRepository) {
        super(repository, bookingMapper);
        this.bookingRepository = repository;
        this.bookingMapper = bookingMapper;
        this.edificeService = edificeService;
        this.bookingAllMapper = bookingAllMapper;
        this.addServiceRepository = addServiceRepository;
    }

    public BookingWithUsersAndServicesDTO rentEdifice(final BookingWithUsersAndServicesDTO booking) {
        EdificeDTO edificeDTO = edificeService.getOne(booking.getEdificeId());
        long rentPeriod = booking.getReturnDate().getDayOfYear() - booking.getRentDate().getDayOfYear();
        booking.setPriceBooking(rentPeriod * edificeDTO.getPriceEdifice());
        booking.setCreatedWhen(LocalDateTime.now());
        booking.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        log.info("VJQ LOG" + booking.toString());

        return bookingAllMapper.toDTO(bookingRepository.save(bookingAllMapper.toEntity(booking)));
    }

    public Page<BookingWithUsersAndServicesDTO> listUserBookingsEdifices(final Long id,
                                                                         final Pageable pageable) {
        Page<Booking> objects = bookingRepository.getBookingByUserId(id, pageable);
        List<BookingWithUsersAndServicesDTO> results = bookingAllMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public Set<BookingWithUsersAndServicesDTO> getAllBookingsByEdifice(Long edfId) {
        return new HashSet<>(bookingAllMapper.toDTOs(bookingRepository.getBookingByEdifice(edfId)));
    }

    public void addServicesOne(AddServiceDTO addServiceDTO, Long bookingId) {
        BookingDTO booking = getOne(bookingId);
        booking.getAddServicesIds().add(addServiceDTO.getId());
        try {
            bookingRepository.updateBookingAddService(addServiceDTO.getId(), bookingId);
        } catch (Exception ignored) {
        }
        try {
            bookingRepository.updatePriceBooking(addServiceRepository.getById(addServiceDTO.getId()).getPriceService(), bookingId);
        } catch (Exception ignored) {
        }
    }

    public void addUserOne(UserDTO userDTO, Long bookingId) {
        BookingDTO booking = getOne(bookingId);
        booking.getUsersIds().add(userDTO.getId());
        try {
            bookingRepository.updateBookingAddUser(userDTO.getId(), bookingId);
        }catch (Exception ignored){
        }
    }

    public Set<BookingWithUsersAndServicesDTO> getAllBookingsByEdificeIdBeforNow(Long edificeId) {
        return new HashSet<>(bookingAllMapper.toDTOs(bookingRepository.getBookingByEdificeIdBeforNow(edificeId)));
    }

    public BookingWithUsersAndServicesDTO getById(Long id) {
        return bookingAllMapper.toDTO(bookingRepository.getOne(id));
    }

    public BookingDTO getById1(Long id) {
        return bookingMapper.toDTO(bookingRepository.getOne(id));
    }

    public Page<BookingWithUsersAndServicesDTO> listBookingsToDay(final Pageable pageable) {
        Page<Booking> objects = bookingRepository.getBookingToDay(pageable);
        List<BookingWithUsersAndServicesDTO> results = bookingAllMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public Page<BookingWithUsersAndServicesDTO> listAllBookingsAfterDay(final Pageable pageable) {
        Page<Booking> objects = bookingRepository.getBookingAfterDay(pageable);
        List<BookingWithUsersAndServicesDTO> results = bookingAllMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public void create(final BookingDTO obj, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        try {
            bookingRepository.updateFileById(fileName, obj.getId());
        }catch (Exception ignore){
        }
    }
}
