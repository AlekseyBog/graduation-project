package com.sber.java13graduationproject.service;

import com.sber.java13graduationproject.constants.Errors;
import com.sber.java13graduationproject.dto.EdificeDTO;
import com.sber.java13graduationproject.dto.EdificeSearchDTO;
import com.sber.java13graduationproject.dto.EdificeWithBookingsDTO;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.mapper.EdificeMapper;
import com.sber.java13graduationproject.mapper.EdificeWithBookingsMapper;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.model.TypeEdifice;
import com.sber.java13graduationproject.repository.EdificeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.AopInvocationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class EdificeService
        extends GenericService<Edifice, EdificeDTO> {

    private final EdificeRepository edificeRepository;

    private final EdificeWithBookingsMapper edificeWithBookingsMapper;


    protected EdificeService(EdificeRepository edificeRepository,
                             EdificeMapper edificeMapper,
                             EdificeWithBookingsMapper edificeWithBookingsMapper) {
        super(edificeRepository, edificeMapper);
        this.edificeRepository = edificeRepository;
        this.edificeWithBookingsMapper = edificeWithBookingsMapper;
    }

    public Page<EdificeWithBookingsDTO> getAllEdificesWithBookings(Pageable pageable) {
        Page<Edifice> edificesPaginated = edificeRepository.findAll(pageable);
        List<EdificeWithBookingsDTO> result = edificeWithBookingsMapper.toDTOs(edificesPaginated.getContent());
        return new PageImpl<>(result, pageable, edificesPaginated.getTotalElements());
    }

    public Set<EdificeWithBookingsDTO> getAllEdificesWithBookings1(Pageable pageable) {
        Page<Edifice> edificesPaginated = edificeRepository.findAll(pageable);
        List<EdificeWithBookingsDTO> result = edificeWithBookingsMapper.toDTOs(edificesPaginated.getContent());
        return new HashSet<>(result);
    }

    public EdificeWithBookingsDTO getEdificeWithBookings(Long id) {
        return edificeWithBookingsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<EdificeWithBookingsDTO> findEdifices(EdificeSearchDTO edificeSearchDTO, Pageable pageable) {
        String typeEdifice = edificeSearchDTO.getTypeEdifice() != null ? String.valueOf(edificeSearchDTO.getTypeEdifice().ordinal()) : null;
        Page<Edifice> edificesPaginated = edificeRepository.searchEdifices(typeEdifice,
                edificeSearchDTO.getEdificeTitle(), pageable);
        List<EdificeWithBookingsDTO> result = edificeWithBookingsMapper.toDTOs(edificesPaginated.getContent());
        return new PageImpl<>(result, pageable, edificesPaginated.getTotalElements());
    }

    public EdificeDTO create(final EdificeDTO object) {
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(edificeRepository.save(mapper.toEntity(object)));
    }

    public EdificeDTO update(final EdificeDTO object) {
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(edificeRepository.save(mapper.toEntity(object)));
    }

    @Override
    public void deleteSoft(Long id) throws MyDeleteException {
        Edifice edifice = edificeRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Объекта с заданным ID = " + id + " не существует"));
        boolean edificeCanBeDeleted = false;
        try {
            edificeRepository.checkEdificeForDeletion(id);// метод возвращает либо false, либо ничего
        } catch (AopInvocationException exception) {
            edificeCanBeDeleted = true;
        }
        if (edificeCanBeDeleted) {
            markAsDeleted(edifice);
            edificeRepository.save(edifice);
        } else {
            throw new MyDeleteException(Errors.Edifices.EDIFICE_DELETE_ERROR);
        }
    }

    public void restore(Long objectId) {
        Edifice edifice = edificeRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Объекта с заданным ID = " + objectId + " не существует"));
        unMarkAsDeleted(edifice);
        edificeRepository.save(edifice);
    }
}
