package com.sber.java13graduationproject.constants;

public interface Errors {
    class Edifices {
        public static final String EDIFICE_DELETE_ERROR = "Объект не может быть удален, так как у него есть активные аренды";
        public static final String EDIFICE_BOOKING_ERROR = "Объект не может быть аренлован, так как у него есть активные аренды";
    }
    
    class AddServices {
        public static final String ADD_SERVICE_DELETE_ERROR = "Услуга не может быть удалена, так как у неё есть активные аренды";
    }


    
    class Users {
        public static final String USER_FORBIDDEN_ERROR = "У вас нет прав просматривать информацию о пользователе";
    }
    
    class REST {
        public static final String DELETE_ERROR_MESSAGE = "Удаление невозможно";
        public static final String AUTH_ERROR_MESSAGE = "Неавторизованный пользователь";
        public static final String ACCESS_ERROR_MESSAGE = "Отказано в доступе!";
        public static final String NOT_FOUND_ERROR_MESSAGE = "Объект не найден!";
    }
}
