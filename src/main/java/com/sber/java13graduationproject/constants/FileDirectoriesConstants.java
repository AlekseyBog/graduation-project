package com.sber.java13graduationproject.constants;

public interface FileDirectoriesConstants {
    String BOOKINGS_UPLOAD_DIRECTORY = "files/bookings";
}
