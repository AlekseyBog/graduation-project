package com.sber.java13graduationproject.constants;

import java.util.List;

public interface SecurityConstants {

    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/static/js/**",
            "/static/**",
            "/static",
            "/static/css/**",
            "/swagger-ui/**",
            "/v3/api-docs/**",
            "/webjars/bootstrap/5.0.2/**",
            "/error",
            "/resources/image/**",
            "/image/**",
            "/image",
            "/");

    List<String> EDIFICES_WHITE_LIST = List.of("/edifices",
            "/edifices/search",
            "/edifices/{id}");

    List<String> EDIFICES_PERMISSION_LIST = List.of("/edifices/add",
            "/edifices/update",
            "/edifices/delete");

    List<String> ADD_SERVICES_WHITE_LIST = List.of("/addServices",
            "/addServices/search",
            "/addServices/{id}");

    List<String> ADD_SERVICES_PERMISSION_LIST = List.of("/addServices/add",
            "/addServices/update",
            "/addServices/delete");

    List<String> USERS_WHITE_LIST = List.of("/login",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password");

    List<String> USERS_PERMISSION_LIST = List.of("/bookings/edifices/*",
            "/bookings/edifices/**",
            "/bookings/**",
            "/bookings/*",
            "/bookings/edifices/addFile",
            "/bookings/edifices/addFile/*",
            "/bookings/download/*"
            );
}
