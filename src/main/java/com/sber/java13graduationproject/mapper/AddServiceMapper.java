package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.AddServiceDTO;
import com.sber.java13graduationproject.dto.UserDTO;
import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.model.User;
import com.sber.java13graduationproject.repository.BookingRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AddServiceMapper
        extends GenericMapper <AddService, AddServiceDTO> {

    private final BookingRepository bookingRepository;

    protected AddServiceMapper(ModelMapper modelMapper, BookingRepository bookingRepository) {
        super(modelMapper, AddService.class, AddServiceDTO.class);
        this.bookingRepository = bookingRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(AddService.class, AddServiceDTO.class)
                .addMappings(m -> m.skip(AddServiceDTO::setBookingsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(AddServiceDTO.class, AddService.class)
                .addMappings(m -> m.skip(AddService::setBookings)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(AddServiceDTO source, AddService destination) {
        if (!Objects.isNull(source.getBookingsIds())) {
            destination.setBookings(new HashSet<>(bookingRepository.findAllById(source.getBookingsIds())));
        }
        else {
            destination.setBookings(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(AddService source, AddServiceDTO destination) {
        destination.setBookingsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(AddService user) {
        return Objects.isNull(user) || Objects.isNull(user.getBookings())
                ? null
                : user.getBookings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
