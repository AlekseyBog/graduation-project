package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.UserDTO;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.model.User;
import com.sber.java13graduationproject.repository.BookingRepository;
import com.sber.java13graduationproject.utils.DateFormatter;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper
        extends GenericMapper<User, UserDTO> {

    private final BookingRepository bookingRepository;

    protected UserMapper(ModelMapper modelMapper, BookingRepository bookingRepository) {
        super(modelMapper, User.class, UserDTO.class);
        this.bookingRepository = bookingRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setBookingsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setBookings)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getBookingsIds())) {
            destination.setBookings(new HashSet<>(bookingRepository.findAllById(source.getBookingsIds())));
        } else {
            destination.setBookings(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setBookingsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(User user) {
        return Objects.isNull(user) || Objects.isNull(user.getBookings())
                ? null
                : user.getBookings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
