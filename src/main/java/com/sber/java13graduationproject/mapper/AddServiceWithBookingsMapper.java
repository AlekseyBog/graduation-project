package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.AddServiceWithBookingsDTO;
import com.sber.java13graduationproject.dto.EdificeWithBookingsDTO;
import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.repository.BookingRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AddServiceWithBookingsMapper
        extends GenericMapper<AddService, AddServiceWithBookingsDTO> {

    private final BookingRepository bookingRepository;

    public AddServiceWithBookingsMapper(ModelMapper modelMapper,
                                     BookingRepository bookingRepository) {
        super(modelMapper, AddService.class, AddServiceWithBookingsDTO.class);
        this.bookingRepository = bookingRepository;
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(AddService.class, AddServiceWithBookingsDTO.class)
                .addMappings(m -> m.skip(AddServiceWithBookingsDTO::setBookingsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(AddServiceWithBookingsDTO.class, AddService.class)
                .addMappings(m -> m.skip(AddService::setBookings)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(AddServiceWithBookingsDTO source, AddService destination) {
        destination.setBookings(new HashSet<>(bookingRepository.findAllById(source.getBookingsIds())));
    }

    @Override
    protected void mapSpecificFields(AddService source, AddServiceWithBookingsDTO destination) {
        destination.setBookingsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(AddService entity) {
        return (Objects.isNull(entity.getBookings()) || Objects.isNull(entity.getId()))
                ? null
                : entity.getBookings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
