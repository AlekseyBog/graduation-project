package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.EdificeDTO;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.repository.BookingRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class EdificeMapper
        extends GenericMapper<Edifice, EdificeDTO> {

    private final BookingRepository bookingRepository;

    public EdificeMapper(ModelMapper modelMapper, BookingRepository bookingRepository) {
        super(modelMapper, Edifice.class, EdificeDTO.class);
        this.bookingRepository = bookingRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Edifice.class, EdificeDTO.class)
                .addMappings(m -> m.skip(EdificeDTO::setBookingsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(EdificeDTO.class, Edifice.class)
                .addMappings(m -> m.skip(Edifice::setBookings)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(EdificeDTO source, Edifice destination) {
        if (!Objects.isNull(source.getBookingsIds())) {
            destination.setBookings(new HashSet<>(bookingRepository.findAllById(source.getBookingsIds())));
        }
        else {
            destination.setBookings(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Edifice source, EdificeDTO destination) {
        destination.setBookingsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Edifice entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getBookings())
                ? null
                : entity.getBookings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

}
