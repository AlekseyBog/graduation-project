package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.GenericDTO;
import com.sber.java13graduationproject.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity (D dto);

    D toDTO (E entity);

    List<E> toEntities(List<D> dtos);

    List<D> toDTOs(List<E> entities);
}
