package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.EdificeWithBookingsDTO;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.repository.BookingRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class EdificeWithBookingsMapper
        extends GenericMapper<Edifice, EdificeWithBookingsDTO> {

    private final BookingRepository bookingRepository;

    public EdificeWithBookingsMapper(ModelMapper modelMapper,
                                     BookingRepository bookingRepository) {
        super(modelMapper, Edifice.class, EdificeWithBookingsDTO.class);
        this.bookingRepository = bookingRepository;
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Edifice.class, EdificeWithBookingsDTO.class)
                .addMappings(m -> m.skip(EdificeWithBookingsDTO::setBookingsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(EdificeWithBookingsDTO.class, Edifice.class)
                .addMappings(m -> m.skip(Edifice::setBookings)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(EdificeWithBookingsDTO source, Edifice destination) {
        destination.setBookings(new HashSet<>(bookingRepository.findAllById(source.getBookingsIds())));
    }

    @Override
    protected void mapSpecificFields(Edifice source, EdificeWithBookingsDTO destination) {
        destination.setBookingsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Edifice entity) {
        return (Objects.isNull(entity.getBookings()) || Objects.isNull(entity.getId()))
                ? null
                : entity.getBookings().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
