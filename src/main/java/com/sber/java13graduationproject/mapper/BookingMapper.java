package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.BookingDTO;
import com.sber.java13graduationproject.model.Booking;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.repository.AddServiceRepository;
import com.sber.java13graduationproject.repository.EdificeRepository;
import com.sber.java13graduationproject.repository.UserRepository;
import com.sber.java13graduationproject.service.EdificeService;
import com.sber.java13graduationproject.utils.DateFormatter;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BookingMapper
        extends GenericMapper<Booking, BookingDTO> {

    private final UserRepository userRepository;

    private final AddServiceRepository addServiceRepository;

    private final EdificeRepository edificeRepository;

    private final EdificeService edificeService;

    public BookingMapper(ModelMapper modelMapper,
                         UserRepository userRepository,
                         AddServiceRepository addServiceRepository,
                         EdificeRepository edificeRepository,
                         EdificeService edificeService) {
        super(modelMapper, Booking.class, BookingDTO.class);
        this.userRepository = userRepository;
        this.addServiceRepository = addServiceRepository;
        this.edificeRepository = edificeRepository;
        this.edificeService = edificeService;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Booking.class, BookingDTO.class)
                .addMappings(m -> m.skip(BookingDTO::setUsersIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingDTO::setAddServicesIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingDTO::setEdificeId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingDTO::setEdificeDTO)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(BookingDTO.class, Booking.class)
                .addMappings(m -> m.skip(Booking::setUsers)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Booking::setAddServices)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Booking::setEdifice)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(BookingDTO source, Booking destination) {
        if (!Objects.isNull(source.getUsersIds())) {
            destination.setUsers(new HashSet<>(userRepository.findAllById(source.getUsersIds())));
        }
        else {
            destination.setUsers(Collections.emptySet());
        }
        if (!Objects.isNull(source.getAddServicesIds())) {
            destination.setAddServices(new HashSet<>(addServiceRepository.findAllById(source.getAddServicesIds())));
        }
        else {
            destination.setAddServices(Collections.emptySet());
        }
        destination.setEdifice(edificeRepository.getReferenceById(source.getEdificeId()));
    }

    @Override
    protected void mapSpecificFields(Booking source, BookingDTO destination) {
        destination.setUsersIds(getIds(source));
        destination.setAddServicesIds(getIds1(source));
        destination.setEdificeId(source.getId());
        destination.setEdificeDTO(edificeService.getOne(source.getEdifice().getId()));
    }

    @Override
    protected Set<Long> getIds(Booking booking) {
        return Objects.isNull(booking) || Objects.isNull(booking.getUsers())
                ? null
                : booking.getUsers().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    protected Set<Long> getIds1(Booking booking) {
        return Objects.isNull(booking) || Objects.isNull(booking.getAddServices())
                ? null
                : booking.getAddServices().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
