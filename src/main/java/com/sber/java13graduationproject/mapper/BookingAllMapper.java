package com.sber.java13graduationproject.mapper;

import com.sber.java13graduationproject.dto.AddServiceWithBookingsDTO;
import com.sber.java13graduationproject.dto.BookingDTO;
import com.sber.java13graduationproject.dto.BookingWithUsersAndServicesDTO;
import com.sber.java13graduationproject.model.AddService;
import com.sber.java13graduationproject.model.Booking;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.repository.AddServiceRepository;
import com.sber.java13graduationproject.repository.BookingRepository;
import com.sber.java13graduationproject.repository.EdificeRepository;
import com.sber.java13graduationproject.repository.UserRepository;
import com.sber.java13graduationproject.service.EdificeService;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class BookingAllMapper
        extends GenericMapper<Booking, BookingWithUsersAndServicesDTO> {

    private final UserRepository userRepository;

    private final AddServiceRepository addServiceRepository;

    private final EdificeRepository edificeRepository;

    private final EdificeService edificeService;

    public BookingAllMapper(ModelMapper modelMapper,
                            UserRepository userRepository,
                            AddServiceRepository addServiceRepository,
                            EdificeRepository edificeRepository,
                            EdificeService edificeService) {
        super(modelMapper, Booking.class, BookingWithUsersAndServicesDTO.class);
        this.userRepository = userRepository;
        this.addServiceRepository = addServiceRepository;
        this.edificeRepository = edificeRepository;
        this.edificeService = edificeService;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Booking.class, BookingWithUsersAndServicesDTO.class)
                .addMappings(m -> m.skip(BookingWithUsersAndServicesDTO::setUsersIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingWithUsersAndServicesDTO::setAddServicesIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingWithUsersAndServicesDTO::setEdificeId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(BookingWithUsersAndServicesDTO::setEdificeDTO)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(BookingWithUsersAndServicesDTO.class, Booking.class)
                .addMappings(m -> m.skip(Booking::setUsers)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Booking::setAddServices)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Booking::setEdifice)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(BookingWithUsersAndServicesDTO source, Booking destination) {
        if (!Objects.isNull(source.getUsersIds())) {
            destination.setUsers(new HashSet<>(userRepository.findAllById(source.getUsersIds())));
        } else {
            destination.setUsers(Collections.emptySet());
        }
        if (!Objects.isNull(source.getAddServicesIds())) {
            destination.setAddServices(new HashSet<>(addServiceRepository.findAllById(source.getAddServicesIds())));
        } else {
            destination.setAddServices(Collections.emptySet());
        }
        destination.setEdifice(edificeRepository.getReferenceById(source.getEdificeId()));
    }

    @Override
    protected void mapSpecificFields(Booking source, BookingWithUsersAndServicesDTO destination) {
        destination.setUsersIds(getIds(source));
        destination.setAddServicesIds(getIds1(source));
        destination.setEdificeId(source.getId());
        destination.setEdificeDTO(edificeService.getOne(source.getEdifice().getId()));
    }

    @Override
    protected Set<Long> getIds(Booking booking) {
        return Objects.isNull(booking) || Objects.isNull(booking.getUsers())
                ? null
                : booking.getUsers().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    protected Set<Long> getIds1(Booking booking) {
        return Objects.isNull(booking) || Objects.isNull(booking.getAddServices())
                ? null
                : booking.getAddServices().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

}
