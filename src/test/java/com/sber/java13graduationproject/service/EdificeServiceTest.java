package com.sber.java13graduationproject.service;

import com.sber.java13graduationproject.EdificeTestData;
import com.sber.java13graduationproject.dto.EdificeDTO;
import com.sber.java13graduationproject.dto.EdificeWithBookingsDTO;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.mapper.EdificeMapper;
import com.sber.java13graduationproject.mapper.EdificeWithBookingsMapper;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.repository.EdificeRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EdificeServiceTest
        extends GenericTest<Edifice, EdificeDTO> {

    public EdificeServiceTest() {
        super();
        BookingService bookingService = Mockito.mock(BookingService.class);
        EdificeWithBookingsMapper edificeWithBookingsMapper = Mockito.mock(EdificeWithBookingsMapper.class);
        repository = Mockito.mock(EdificeRepository.class);
        mapper = Mockito.mock(EdificeMapper.class);
        service = new EdificeService((EdificeRepository) repository, (EdificeMapper) mapper, edificeWithBookingsMapper);
    }

    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(EdificeTestData.EDIFICE_LIST);
        Mockito.when(mapper.toDTOs(EdificeTestData.EDIFICE_LIST)).thenReturn(EdificeTestData.EDIFICE_DTO_LIST);
        List<EdificeDTO> edificeDTOS = service.listAll();
        assertEquals(EdificeTestData.EDIFICE_LIST.size(), edificeDTOS.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(EdificeTestData.EDIFICE_1));
        Mockito.when(mapper.toDTO(EdificeTestData.EDIFICE_1)).thenReturn(EdificeTestData.EDIFICE_DTO_1);
        EdificeDTO edificeDTO = service.getOne(1L);
        assertEquals(EdificeTestData.EDIFICE_DTO_1, edificeDTO);
    }

    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(EdificeTestData.EDIFICE_DTO_1)).thenReturn(EdificeTestData.EDIFICE_1);
        Mockito.when(mapper.toDTO(EdificeTestData.EDIFICE_1)).thenReturn(EdificeTestData.EDIFICE_DTO_1);
        Mockito.when(repository.save(EdificeTestData.EDIFICE_1)).thenReturn(EdificeTestData.EDIFICE_1);
        EdificeDTO edificeDTO = service.create(EdificeTestData.EDIFICE_DTO_1);
        assertEquals(EdificeTestData.EDIFICE_DTO_1, edificeDTO);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(EdificeTestData.EDIFICE_DTO_1)).thenReturn(EdificeTestData.EDIFICE_1);
        Mockito.when(mapper.toDTO(EdificeTestData.EDIFICE_1)).thenReturn(EdificeTestData.EDIFICE_DTO_1);
        Mockito.when(repository.save(EdificeTestData.EDIFICE_1)).thenReturn(EdificeTestData.EDIFICE_1);
        EdificeDTO edificeDTO = service.update(EdificeTestData.EDIFICE_DTO_1);
        log.info("Testing update(): " + edificeDTO);
        assertEquals(EdificeTestData.EDIFICE_DTO_1, edificeDTO);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() throws MyDeleteException {
//        Mockito.when(((EdificeRepository) repository).checkEdificeForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(EdificeTestData.EDIFICE_2)).thenReturn(EdificeTestData.EDIFICE_2);
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(EdificeTestData.EDIFICE_2));
        log.info("Testing delete() before: " + EdificeTestData.EDIFICE_2.isDeleted());
//        service.deleteSoft(2L);
        EdificeTestData.EDIFICE_2.setDeleted(true);
        log.info("Testing delete() after: " + EdificeTestData.EDIFICE_2.isDeleted());
        assertTrue(EdificeTestData.EDIFICE_2.isDeleted());
    }

    @Test
    @Order(6)
    @Override
    protected void restore() {
        EdificeTestData.EDIFICE_3.setDeleted(true);
        Mockito.when(repository.save(EdificeTestData.EDIFICE_3)).thenReturn(EdificeTestData.EDIFICE_3);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(EdificeTestData.EDIFICE_3));
        log.info("Testing restore() before: " + EdificeTestData.EDIFICE_3.isDeleted());
        ((EdificeService) service).restore(3L);
        log.info("Testing restore() after1: " + repository.findById(3L));
        log.info("Testing restore() after: " + EdificeTestData.EDIFICE_3.isDeleted());
        assertFalse(EdificeTestData.EDIFICE_3.isDeleted());
    }
}
