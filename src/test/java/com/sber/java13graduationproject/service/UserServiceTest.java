package com.sber.java13graduationproject.service;

import com.sber.java13graduationproject.dto.UserDTO;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.model.User;
import org.junit.jupiter.api.Test;

public class UserServiceTest extends GenericTest<User, UserDTO> {
    
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Test
    @Override
    protected void restore() {
    
    }

}
