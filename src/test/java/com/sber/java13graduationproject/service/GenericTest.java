package com.sber.java13graduationproject.service;

import com.sber.java13graduationproject.dto.GenericDTO;
import com.sber.java13graduationproject.exception.MyDeleteException;
import com.sber.java13graduationproject.mapper.GenericMapper;
import com.sber.java13graduationproject.model.GenericModel;
import com.sber.java13graduationproject.repository.GenericRepository;
import com.sber.java13graduationproject.service.userdetails.CustomUserDetails;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO> {

    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapper<E, D> mapper;

    @BeforeEach
    void init() {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(CustomUserDetails
                .builder()
                .username("USER"),
                null,
                null);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    protected abstract void getAll();

    protected abstract void getOne();

    protected abstract void create();

    protected abstract void update();

    protected abstract void delete() throws MyDeleteException;

    protected abstract void restore();
}
