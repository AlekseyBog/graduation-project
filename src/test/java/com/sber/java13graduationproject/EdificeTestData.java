package com.sber.java13graduationproject;

import com.sber.java13graduationproject.dto.EdificeDTO;
import com.sber.java13graduationproject.model.Edifice;
import com.sber.java13graduationproject.model.TypeEdifice;
import com.sber.java13graduationproject.model.TypePrice;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface EdificeTestData {

    EdificeDTO EDIFICE_DTO_1 = new EdificeDTO(
            "dom1",
            TypeEdifice.HOUSE,
            "dom1",
            TypePrice.PER_DAY,
            100L, new HashSet<>(),false);

    EdificeDTO EDIFICE_DTO_2 = new EdificeDTO(
            "dom2",
            TypeEdifice.HOUSE,
            "dom2",
            TypePrice.PER_DAY,
            1000L, new HashSet<>(),false);

    EdificeDTO EDIFICE_DTO_3_DELETED = new EdificeDTO(
            "dom3",
            TypeEdifice.HOUSE,
            "dom3",
            TypePrice.PER_DAY,
            100000L, new HashSet<>(),true);

    List<EdificeDTO> EDIFICE_DTO_LIST = Arrays.asList(EDIFICE_DTO_1, EDIFICE_DTO_2, EDIFICE_DTO_3_DELETED);


    Edifice EDIFICE_1 = new Edifice("dom1",
            TypeEdifice.HOUSE,
            "dom1",
            TypePrice.PER_DAY,
            100L,
            null);

    Edifice EDIFICE_2 = new Edifice(
            "dom2",
            TypeEdifice.HOUSE,
            "dom2",
            TypePrice.PER_DAY,
            10032425L,
            null);

    Edifice EDIFICE_3 = new Edifice("dom3",
            TypeEdifice.HOUSE,
            "dom3",
            TypePrice.PER_DAY,
            1093480L,
            null);


    List<Edifice> EDIFICE_LIST = Arrays.asList(EDIFICE_1, EDIFICE_2, EDIFICE_3);
}
